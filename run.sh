export $(cat .env)
./.heroku/python/bin/poetry config virtualenvs.create false
./.heroku/python/bin/poetry install --no-dev
./.heroku/python/bin/alembic upgrade head

./.heroku/python/bin/pip uninstall googletrans==4.0.0-rc1

./.heroku/python/bin/pip install googletrans==3.1.0a0

./.heroku/python/bin/uvicorn app.main:app --host=0.0.0.0 --port=${PORT:-8000}
