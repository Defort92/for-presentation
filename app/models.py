from datetime import datetime

from sqlalchemy import Column, Integer, String, TIMESTAMP, text, ForeignKey, Date, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy_utils import URLType

from app.database import Base


#  Users --------------------------------------------------------------------


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, nullable=False)
    email = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    is_admin = Column(Boolean, nullable=False, default=False)
    # created_at = Column(TIMESTAMP(timezone=True), nullable=False, server_default=text("now()"))

    user_profile = relationship("UserProfile", back_populates="user")
    user_rating = relationship("UserRating", back_populates="user")
    user_purposes = relationship("Purpose", back_populates="user")
    user_collections = relationship("Collection", back_populates="user")
    user_words = relationship("UserWord", back_populates="user")


class UserProfile(Base):
    __tablename__ = "user_profiles"

    user_id = Column(Integer, ForeignKey(User.id, ondelete="CASCADE"), primary_key=True)
    name = Column(String, nullable=True)
    surname = Column(String, nullable=True)
    city = Column(String, nullable=True)
    phone = Column(String, nullable=True)
    photo = Column(URLType, nullable=True)
    score = Column(Integer, default=0)
    english_level = Column(String, nullable=True)

    user = relationship("User", back_populates="user_profile")


class UserRating(Base):
    __tablename__ = "user_ratings"

    id = Column(Integer, primary_key=True, nullable=False)
    user_id = Column(Integer, ForeignKey(User.id, ondelete="CASCADE"), unique=True)
    place = Column(Integer, nullable=False)

    user = relationship("User", back_populates="user_rating")


class Purpose(Base):
    __tablename__ = "purposes"

    id = Column(Integer, primary_key=True, nullable=False)
    user_id = Column(Integer, ForeignKey(User.id, ondelete="CASCADE"))
    name = Column(String, nullable=True)
    description = Column(String, nullable=True)
    words_amount = Column(Integer, nullable=True)
    completed = Column(Boolean, nullable=False, default=False)
    date = Column(Date, nullable=True)

    user = relationship("User", back_populates="user_purposes")


#  Words --------------------------------------------------------------------


class Word(Base):
    __tablename__ = "words"

    id = Column(Integer, primary_key=True, nullable=False)
    word_ru = Column(String, nullable=False)
    word_en = Column(String, nullable=False)
    description = Column(String, nullable=True)

    word_users = relationship("UserWord", back_populates="word")
    word_collections = relationship("CollectionWord", back_populates="word")
    word_rating = relationship("WordRating", back_populates="word")


class WordRating(Base):
    __tablename__ = "word_ratings"

    id = Column(Integer, primary_key=True, nullable=False)
    word_id = Column(Integer, ForeignKey(Word.id, ondelete="CASCADE"))
    place = Column(Integer, nullable=False)

    word = relationship("Word", back_populates="word_rating")


#  Collections --------------------------------------------------------------------


class Collection(Base):
    __tablename__ = "collections"

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    description = Column(String, nullable=True)
    user_id = Column(Integer, ForeignKey(User.id, ondelete="CASCADE"))

    user = relationship("User", back_populates="user_collections")
    collection_words = relationship("CollectionWord", back_populates="collection")


#  Connections --------------------------------------------------------------------


class UserWord(Base):
    __tablename__ = "user_word"

    user_id = Column(ForeignKey(User.id), primary_key=True)
    word_id = Column(ForeignKey(Word.id), primary_key=True)
    count_of_repetitions = Column(Integer, nullable=False)

    user = relationship("User", back_populates="user_words")
    word = relationship("Word", back_populates="word_users")


class CollectionWord(Base):
    __tablename__ = "collection_word"

    collection_id = Column(ForeignKey(Collection.id), primary_key=True)
    word_id = Column(ForeignKey(Word.id), primary_key=True)

    collection = relationship("Collection", back_populates="collection_words")
    word = relationship("Word", back_populates="word_collections")


#  Challenges --------------------------------------------------------------------


class Challenge(Base):
    __tablename__ = "challenges"

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    description = Column(String, nullable=True)
    words_count = Column(Integer, nullable=False, default=10)
    time_limit = Column(Integer, nullable=False, default=10 * 60)  # time in seconds
    score = Column(Integer, nullable=False, default=100)
