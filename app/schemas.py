from datetime import datetime
from typing import Optional

from pydantic import BaseModel, EmailStr


class UserOut(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime

    class Config:
        orm_mode = True


class UserCreate(BaseModel):
    email: EmailStr
    password: str


class UserProfile(BaseModel):
    name: str = None
    surname: str = None
    city: str = None
    phone: str = None
    photo: str = None
    score: int = 0
    english_level: str = None


class UserProfileUpdate(BaseModel):
    name: Optional[str] = None
    surname: Optional[str] = None
    city: Optional[str] = None
    phone: Optional[str] = None
    english_level: Optional[str] = None
    photo: Optional[str] = None


class UserLogin(BaseModel):
    email: EmailStr
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None


class TextToTranslate(BaseModel):
    content: Optional[str] = None
    src: Optional[str] = None
    dest: Optional[str] = None


class ToFavorites(BaseModel):
    en: Optional[str] = None
    ru: Optional[str] = None


class FavoriteWordCreate(BaseModel):
    word_en: str
    word_ru: str


class UserWord(BaseModel):
    user_id: int
    word_id: int


class Word(BaseModel):
    id: int


class Scores(BaseModel):
    score: int


class Purpose(BaseModel):
    user_id: int
    description: str
    words_amount: int
    date: Optional[datetime] = None


class AddPurpose(BaseModel):
    description: str
    date: Optional[datetime] = None


class DeletePurpose(BaseModel):
    id: int


class ChangePurpose(BaseModel):
    id: int
    description: str


class Settings(BaseModel):
    authjwt_secret_key: str = "secret"
    # Configure application to store and get JWT from cookies
    authjwt_token_location: set = {"cookies"}
    # Only allow JWT cookies to be sent over https
    authjwt_cookie_secure: bool = False
    # Enable csrf double submit protection. default is True
    authjwt_cookie_csrf_protect: bool = True


class Collection(BaseModel):
    id = int
    name = str
    description = str
    user_id = int


class CollectionUpdate(BaseModel):
    name = str
    description = str
    user_id = int


class CollectionCreate(BaseModel):
    name = str
    description = str
    user_id = int


class CollectionsWords(BaseModel):
    collection_id = int
    word_id = int


class DeleteBody(BaseModel):
    id: str


class SaveBody(BaseModel):
    collection_id: str
    word: str


class SaveBodyResponse(BaseModel):
    type: str
    collection_id: str
    word_id: str
    word_ru: str
    word_en: str


class DeleteWordBody(BaseModel):
    collection_id: str
    word_id: str


class SimpleResponse(BaseModel):
    type: str


class RegisterValidator(BaseModel):
    name: str
