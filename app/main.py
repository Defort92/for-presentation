from pathlib import Path

from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi_jwt_auth.exceptions import AuthJWTException
from sqlalchemy.orm import Session
from starlette.middleware.sessions import SessionMiddleware
from starlette.requests import Request

from app.config import settings
from app.database import get_db
from app.oauth2 import get_current_user
from app.routers import user, auth, translate, collections, rating, chat

# ! Alembic is going to handle this portion
# models.Base.metadata.create_all(bind=engine)

app = FastAPI()

BASE_PATH = Path(__file__).resolve().parent
app.mount("/static", StaticFiles(directory=str(BASE_PATH / "static")), name="static")
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))

origins = ["*"]  # list of domains / urls that can talk to my API to allow all domains to access set origins to ["*"]
SECRET_KEY = settings.SECRET_KEY
app.add_middleware(SessionMiddleware, secret_key=SECRET_KEY)
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(user.router)
app.include_router(auth.router)
app.include_router(translate.router)
app.include_router(collections.router)
app.include_router(rating.router)
app.include_router(chat.router)


@app.get("/", response_class=HTMLResponse)
async def root(request: Request, db: Session = Depends(get_db)):
    """Функция отображает первоначальную страницу"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        current_user = None
    return templates.TemplateResponse("index.html", {"request": request, "user": current_user})


@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    """Функция выводит ошибку, если с jwt токеном что-то пошло не так"""
    return JSONResponse(status_code=exc.status_code, content={"detail": exc.message})
