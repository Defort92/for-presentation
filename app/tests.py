import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

from app import models
from app.database import get_db, Base
from app.main import app
from app.oauth2 import verify_password
from app.routers.rating import refresh_words, refresh_users

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def override_get_db():
    connection = engine.connect()
    transaction = connection.begin()
    session = TestingSessionLocal()
    try:
        yield session
    finally:
        session.close()
        transaction.rollback()
        connection.close()


@pytest.fixture(scope="module")
def test_db():
    Base.metadata.create_all(bind=engine)
    yield TestingSessionLocal()
    Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope="module")
def client() -> TestClient:
    """Return test client"""
    _app = app
    _app.dependency_overrides[get_db] = override_get_db
    with TestClient(_app) as client:
        yield client


def test_get_main(client: TestClient):
    """Test get main page"""
    response = client.get("/")
    assert response.status_code == 200


def test_get_translate(client: TestClient):
    """Test get translate page"""
    response = client.get("/translate")
    assert response.status_code == 200


def test_get_collection(client: TestClient, test_db):
    """Test get collection page"""
    response = client.get("/collection/all")
    assert response.status_code == 200


def test_update_collection_without_auth(client: TestClient, test_db: Session):
    """Test update collection by id without auth"""
    response = client.get("/collection/update_collection/0")
    assert response.status_code == 200


def test_get_collection_by_id_without_auth(client: TestClient):
    """Test get collection page by id without auth"""
    response = client.get("/collection/collection/0")
    assert response.status_code == 200


def test_add_favorites_words_without_auth(client: TestClient):
    """Test add words to favorites without auth"""
    data = {"en": "test", "ru": "тест"}
    response = client.post("/translate/add_to_favorites", json=data)
    assert response.status_code == 200
    assert response.json() == "Not allowed"


def test_update_collection_without_auth_post(client: TestClient):
    """Test update collection by id without auth"""
    data = {"name": "test"}
    response = client.post("/collection/update_collection/0", data=data)
    assert response.status_code == 303


def test_delete_collection_without_auth(client: TestClient, test_db: Session):  # check
    """Test delete collection by id without auth"""
    data = {"name": "test"}
    response = client.post("/collection/delete_collection/0", data=data)
    assert response.status_code == 404


def test_create_collection_without_auth(client: TestClient):
    """Test create collection without auth"""
    response = client.get("/collection/create_collection")
    assert response.status_code == 200


def test_add_word_to_collection_without_auth(client: TestClient):
    """Test add word to collection without auth"""
    data = {"collection_id": "1", "word": "test"}
    response = client.post("/collection/add_word", json=data)
    assert response.status_code == 303


def test_delete_word_from_collection_without_auth(client: TestClient):
    """Test delete word from collection without auth"""
    data = {"collection_id": "1", "word_id": "1"}
    response = client.post("/collection/delete_word_from_collection", json=data)
    assert response.json() == {"type": "error"}


def test_get_login(client: TestClient):
    """Test get login page"""
    response = client.get("/auth/sign_in")
    assert response.status_code == 200


def test_get_register(client: TestClient):
    """Test get register page"""
    response = client.get("/auth/sign_up")
    assert response.status_code == 200


def test_updating_words_rating(test_db: Session):
    """Test refreshing table users rating"""
    try:
        assert refresh_words(test_db) is None
    except Exception as e:
        raise e


def test_updating_users_rating(test_db: Session):
    """Test refreshing table words rating"""
    try:
        assert refresh_users(test_db) is None
    except Exception as e:
        raise e


def test_get_rating_users(client: TestClient, test_db):
    """Test get rating users page"""
    response = client.get("/rating/users")
    assert response.status_code == 200


def test_get_rating_words(client: TestClient, test_db):
    """Test get rating words page"""
    response = client.get("/rating/words")
    assert response.status_code == 200


def test_get_chat_register(client: TestClient):
    """Test get chat register page without auth"""
    response = client.get("/chat/register")
    assert response.status_code == 200
    assert response.url[17:] == "/auth/sign_in"


def test_get_chat(client: TestClient):
    """Test get chat page without auth"""
    response = client.get("/chat")
    assert response.status_code == 200
    assert response.url[17:] == "/auth/sign_in"


def test_get_profile(client: TestClient):
    """Test get profile page without auth"""
    response = client.get("/user/profile")
    assert response.status_code == 200
    assert response.url[17:] == "/auth/sign_in"


def test_get_profile_update_without_auth(client: TestClient):
    """Test get profile update page without auth"""
    response = client.get("/user/profile/update")
    assert response.status_code == 200
    assert response.url[17:] == "/auth/sign_in"


def test_get_favorites(client: TestClient):
    """Test get favorites words page without auth"""
    response = client.get("/user/profile/favorites")
    assert response.status_code == 200
    assert response.url[17:] == "/auth/sign_in"


def test_change_purpose_without_auth(client: TestClient, test_db: Session):
    """Test change purpose description without auth"""
    data = {"id": 1, "description": "test_description"}
    response = client.post("/user/profile/purposes/change", json=data)
    assert response.status_code == 303


def test_add_purpose_without_auth(client: TestClient, test_db: Session):
    """Test add purpose without auth"""
    data = {"description": "test", "date": None}
    response = client.post("/user/profile/purposes/add", json=data)
    assert response.status_code == 303


def test_get_favorites_repeat_without_auth(client: TestClient):
    """Test get favorites words repeat page without auth"""
    response = client.get("/user/profile/favorites/repeat")
    assert response.status_code == 200
    assert response.url[17:] == "/auth/sign_in"


def test_complete_purpose_without_auth(client: TestClient, test_db: Session):
    """Test complete purpose without auth"""

    data = {"id": 1}
    response = client.post("/user/profile/purposes/complete", json=data)
    assert response.status_code == 303


def test_get_purposes(client: TestClient):
    """Test get purposes page without auth"""
    response = client.get("/user/profile/purposes")
    assert response.status_code == 200
    assert response.url[17:] == "/auth/sign_in"


def test_delete_purpose_without_auth(client: TestClient, test_db: Session):
    """Test delete purpose without auth"""

    data = {"id": 1}
    response = client.post("/user/profile/purposes/delete", json=data)
    assert response.status_code == 303


def test_change_password_post_without_auth(client: TestClient, test_db: Session):
    """Test change user password without auth"""
    data = {"old_pass": "test123", "new_pass": "test12345", "new_pass_repeat": "test12345"}
    response = client.post("/user/profile/change_pass", data=data)
    assert response.status_code == 303


def test_get_change_pass(client: TestClient):
    """Test get change password page without auth"""
    response = client.get("/user/profile/change_pass")
    assert response.status_code == 200
    assert response.url[17:] == "/auth/sign_in"


def test_register(client: TestClient, test_db: Session):
    """Test register user"""
    data = {"email": "test@mail.ru", "password": "test123"}
    response = client.post("/auth/sign_up", data=data)
    assert response.status_code == 303

    user = test_db.query(models.User).first()
    assert user.email == "test@mail.ru"
    assert verify_password(data["password"], user.password)


def test_change_password_post_without_auth(client: TestClient, test_db: Session):
    """Test change password without auth"""
    data = {"old_pass": "test123", "new_pass": "test123456", "new_pass_repeat": "test123456"}
    response = client.post("/user/profile/change_pass", data=data)
    assert response.status_code == 303


def test_delete_favorite_word_without_auth(client: TestClient, test_db: Session):
    """Test delete word from favorites without auth"""
    data = {"id": 1}
    response = client.post("/user/profile/favorites/delete", json=data)
    assert response.status_code == 303


def test_update_profile_without_auth(client: TestClient, test_db: Session):
    """Test update user profile without auth"""
    data = {
        "name": "test1",
    }
    response = client.post("/user/profile/update", data=data)
    assert response.status_code == 303


def test_add_scores_without_auth(client: TestClient, test_db: Session):
    """Test add scores to user without auth"""
    data = {"score": 100}
    response = client.post("/user/profile/scores/add", json=data)
    assert response.status_code == 303


def test_login_with_incorrect_credentials(client: TestClient):
    """Test login user with incorrect credentials"""
    data = {"email": "test2@mail.ru", "password": "test123"}
    response = client.post("/auth/sign_in", data=data)
    assert response.status_code == 404


def test_login_with_correct_credentials(client: TestClient):
    """Test login user with correct credentials"""
    data = {"email": "test@mail.ru", "password": "test123"}
    response = client.post("/auth/sign_in", data=data)
    assert response.status_code == 303


def test_get_profile_with_auth(client: TestClient):
    """Test get profile page with auth"""
    response = client.get("user/profile")
    assert response.status_code == 200


def test_get_profile_update_with_auth(client: TestClient):
    """Test get profile update page with auth"""
    response = client.get("/user/profile/update")
    assert response.status_code == 200


def test_get_favorites_repeat_with_auth(client: TestClient):
    """Test get favorites words repeat page with auth"""
    response = client.get("/user/profile/favorites/repeat")
    assert response.status_code == 200


def test_get_change_pass_with_auth(client: TestClient):
    """Test get change password page with auth"""
    response = client.get("/user/profile/change_pass")
    assert response.status_code == 200


def test_get_favorites_with_auth(client: TestClient):
    """Test get favorites words page with auth"""
    response = client.get("/user/profile/favorites")
    assert response.status_code == 200
    assert response.url[17:] != "/auth/sign_in"


def test_update_profile_with_auth(client: TestClient, test_db: Session):
    """Test update user profile with auth"""
    data = {
        "name": "test1",
    }
    response = client.post("/user/profile/update", data=data)
    assert response.status_code == 303

    user = test_db.query(models.User).first()
    profile = test_db.query(models.UserProfile).filter(models.UserProfile.user_id == user.id).first()
    assert profile.name == data["name"]


def test_update_profile_with_auth_with_incorrect_data(client: TestClient, test_db: Session):
    """Test update user profile with auth with incorrect data"""
    data = {"name": "test1", "phone": "incorrect phone"}
    response = client.post("/user/profile/update", data=data)
    assert response.status_code == 400


def test_add_favorites_words_with_auth(client: TestClient, test_db: Session):
    """Test add words to favorites with auth"""
    data = {"en": "test", "ru": "тест"}
    response = client.post("/translate/add_to_favorites", json=data)
    assert response.status_code == 200

    user = test_db.query(models.User).first()
    word = test_db.query(models.UserWord).filter(models.UserWord.user_id == user.id).first()
    assert word.word.word_en == data["en"]
    assert word.word.word_ru == data["ru"]


def test_add_favorites_words_that_already_in_with_auth(client: TestClient, test_db: Session):
    """Test add words to favorites that already in with auth"""
    data = {"en": "test", "ru": "тест"}
    response = client.post("/translate/add_to_favorites", json=data)
    assert response.status_code == 200
    assert response.json() == "already in favorites"


def test_delete_favorite_word(client: TestClient, test_db: Session):
    """Test delete word from favorites"""
    user = test_db.query(models.User).first()
    word = test_db.query(models.UserWord).filter(models.UserWord.user_id == user.id).first()
    data = {"id": word.word.id}
    response = client.post("/user/profile/favorites/delete", json=data)
    assert response.status_code == 200

    word = test_db.query(models.UserWord).filter(models.UserWord.user_id == user.id).first()
    assert word is None


def test_add_scores(client: TestClient, test_db: Session):
    """Test add scores to user"""
    data = {"score": 100}
    response = client.post("/user/profile/scores/add", json=data)
    user = test_db.query(models.User).first()
    user_profile = test_db.query(models.UserProfile).filter(models.UserProfile.user_id == user.id).first()
    assert user_profile.score == data["score"]
    assert response.status_code == 200


def test_get_purposes_with_auth(client: TestClient):
    """Test get purposes page with auth"""
    response = client.get("/user/profile/purposes")
    assert response.status_code == 200


def test_add_purpose_with_auth(client: TestClient, test_db: Session):
    """Test add purpose with auth"""
    data = {"description": "test", "date": None}
    response = client.post("/user/profile/purposes/add", json=data)
    assert response.status_code == 200

    user = test_db.query(models.User).first()
    purpose = test_db.query(models.Purpose).filter(models.Purpose.user_id == user.id).first()
    assert purpose.description == data["description"]


def test_complete_purpose_with_auth(client: TestClient, test_db: Session):
    """Test complete purpose with auth"""

    user = test_db.query(models.User).first()
    purpose = test_db.query(models.Purpose).filter(models.Purpose.user_id == user.id).first()
    assert purpose.completed is False

    data = {"id": purpose.id}
    response = client.post("/user/profile/purposes/complete", json=data)
    assert response.status_code == 200


def test_change_purpose(client: TestClient, test_db: Session):
    """Test change purpose description"""

    user = test_db.query(models.User).first()
    purpose = test_db.query(models.Purpose).filter(models.Purpose.user_id == user.id).first()
    data = {"id": purpose.id, "description": "test_description"}
    response = client.post("/user/profile/purposes/change", json=data)
    assert response.status_code == 200


def test_delete_purpose(client: TestClient, test_db: Session):
    """Test delete purpose"""

    user = test_db.query(models.User).first()
    purpose = test_db.query(models.Purpose).filter(models.Purpose.user_id == user.id).first()
    data = {"id": purpose.id}
    response = client.post("/user/profile/purposes/delete", json=data)
    purpose = test_db.query(models.Purpose).filter(models.Purpose.user_id == user.id).first()
    assert purpose is None
    assert response.status_code == 200


def test_change_password_post(client: TestClient, test_db: Session):
    """Test change user password"""
    data = {"old_pass": "test123", "new_pass": "test12345", "new_pass_repeat": "test12345"}
    response = client.post("/user/profile/change_pass", data=data)
    assert response.status_code == 200

    user = test_db.query(models.User).first()
    assert verify_password(plain_password=data["new_pass"], hashed_password=user.password)


def test_change_password_post_incorrect_credentials(client: TestClient, test_db: Session):
    """Test change user password given incorrect credentials"""
    data = {"old_pass": "test123", "new_pass": "test123456", "new_pass_repeat": "test123456"}
    response = client.post("/user/profile/change_pass", data=data)
    assert response.status_code == 400


def test_get_create_collection_with_auth(client: TestClient, test_db: Session):
    """Test create collection page with auth"""
    response = client.get("/collection/create_collection")
    assert response.status_code == 200


def test_create_collection_incorrect_data(client: TestClient):
    """Test create collection with incorrect data with auth"""
    data = {"name": None, "description": "test description", "photo": None}
    response = client.post("/collection/create_collection", data=data)
    assert response.status_code == 400


def test_create_collection_correct_data(client: TestClient):
    """Test create collection with correct data with auth"""
    data = {"name": "test", "description": "test description", "photo": None}
    response = client.post("/collection/create_collection", data=data)
    assert response.status_code == 303


def test_get_collection_by_id_with_auth(client: TestClient):
    """Test collection page"""
    response = client.get("/collection/collection/0")
    assert response.status_code == 200


def test_update_collection_with_auth_get(client: TestClient):
    """Test update collection by id with auth"""
    response = client.get("/collection/update_collection/0")
    assert response.status_code == 200


def test_update_collection_with_auth_with_incorrect_data_post(client: TestClient):
    """Test update collection by id with incorrect data with auth"""
    data = {"name": None}
    response = client.post("/collection/update_collection/0", data=data)
    assert response.status_code == 302


def test_update_collection_with_auth_with_correct_data_post(client: TestClient, test_db: Session):
    """Test update collection by id with correct data with auth"""
    data = {"name": "test1"}
    response = client.post("/collection/update_collection/0", data=data)
    assert response.status_code == 200


def test_delete_collection_by_id_with_auth(client: TestClient, test_db: Session):
    """Test delete collection by id with auth"""
    collection = models.Collection(id=2, name="Test", description="Test_desc", user_id=0)
    test_db.add(collection)
    test_db.commit()
    data = {"id": collection.id}
    response = client.post("/collection/delete_collection", json=data)
    assert response.status_code == 200
    assert response.json() == "success"

    collection = test_db.query(models.Collection).filter(models.Collection.id == data["id"]).first()
    assert collection is None


def test_delete_collection_by_id_with_auth_error(client: TestClient):
    """Test delete collection by id with auth with error"""

    data = {"id": 0}
    response = client.post("/collection/delete_collection", json=data)
    assert response.status_code == 200
    assert response.json() == "error"


def test_add_word_to_collection_with_auth(client: TestClient, test_db: Session):
    """Test add word to collection with auth"""
    collection = models.Collection(id=3, name="Test", description="Test_desc", user_id=0)
    test_db.add(collection)
    test_db.commit()
    data = {"collection_id": "3", "word": "test"}
    response = client.post("/collection/add_word", json=data)
    assert response.status_code == 200
    assert response.json() == {
        "type": "success",
        "collection_id": "3",
        "word_id": 1,
        "word_ru": "тест",
        "word_en": "test",
    }


def test_add_word_to_collection_with_auth_word_exists(client: TestClient, test_db: Session):
    """Test add word to collection with auth with existing word"""
    data = {"collection_id": "3", "word": "test"}
    response = client.post("/collection/add_word", json=data)
    assert response.status_code == 200
    assert response.json() == {"type": "not added", "collection_id": 3, "word_ru": "тест", "word_en": "test"}


def test_delete_word_from_collection_with_auth(client: TestClient):
    """Test delete word from collection with auth"""
    data = {"collection_id": "3", "word_id": "1"}
    response = client.post("/collection/delete_word_from_collection", json=data)
    assert response.status_code == 200
    assert response.json() == {"type": "success"}


def test_delete_word_from_collection_with_auth_with_incorrect_data(client: TestClient):
    """Test delete word from collection with auth with incorrect data"""
    data = {"collection_id": "1", "word_id": "1"}
    response = client.post("/collection/delete_word_from_collection", json=data)
    assert response.status_code == 200
    assert response.json() == {"type": "error"}


def test_translate(client: TestClient):
    """Test translate word"""
    data = {"content": "test", "src": "en", "dest": "ru"}
    response = client.post("/translate", json=data)
    assert response.status_code == 307


def test_get_chat_register_with_auth(client: TestClient):
    """Test get chat register page with auth"""
    response = client.get("/chat/register")
    assert response.status_code == 200


def test_get_chat_with_auth(client: TestClient):
    """Test get chat page with auth"""
    response = client.get("/chat")
    assert response.status_code == 200


def test_register_user_to_chat(client: TestClient):
    """Test register user to chat with auth"""
    data = {"name": "testname"}
    response = client.post("/chat/api/register", json=data)
    assert response.status_code == 200
    assert data["name"] in response.headers.get("set-cookie")


def test_get_user_from_cookies(client: TestClient):
    """Test get current user from cookies"""
    response = client.get("/chat/api/current_user")
    assert response.status_code == 200
    assert response.json() == "testname"


def test_websocket(client: TestClient):
    """Test websocket connection"""
    with client.websocket_connect("/chat-api") as websocket:
        data = websocket.receive_json()
        assert data == {"sender": "testname", "message": "got connected"}


def test_register_with_same_email(client: TestClient, test_db: Session):
    """Test register user with already existed email"""
    data = {"email": "test@mail.ru", "password": "test123"}
    response = client.post("/auth/sign_up", data=data)
    assert response.status_code == 403


def test_register_with_invalid_form(client: TestClient):
    """Test register user with invalid form"""
    data = {"email": "test", "password": "test123"}
    response = client.post("/auth/sign_up", data=data)
    assert response.status_code == 404


def test_signin_with_invalid_form(client: TestClient):
    """Test sign in user with invalid form"""
    data = {"email": "test", "password": "test123"}
    response = client.post("/auth/sign_in", data=data)
    assert response.status_code == 401


def test_logout_user(client: TestClient):
    """Test logout user"""
    response = client.get("/auth/logout")
    assert response.status_code == 200
    assert response.url[17:] == "/"


def test_google_oauth_redirect(client: TestClient):
    """Test google oauth redirect"""
    response = client.get("/auth/oauth")
    assert "https://accounts.google.com/o/oauth2/v2" in response.url


def test_google_get_token_page(client: TestClient):
    """Test token page"""
    response = client.get("/auth/token")
    assert response.status_code == 200


def test_google_get_token_processing(client: TestClient):
    """Test working with token"""
    response = client.get("/auth/token?token=test_oauth")
    assert response.json() == {"msg": "Successfully login"}


def test_google_get_token_processing_with_existed_user(client: TestClient):
    """Test working with token with existed user"""
    response = client.get("/auth/token?token=test_oauth")
    assert response.json() == {"msg": "Successfully login"}
