from typing import List, Optional
import re

from fastapi import Request, UploadFile, File

from app.oauth2 import verify_password


class AuthForm:
    def __init__(self, request: Request):
        self.request: Request = request
        self.errors: List = []
        self.email: Optional[str] = None
        self.password: Optional[str] = None

    async def load_data(self):
        form = await self.request.form()
        self.email = form.get("email")
        self.password = form.get("password")

    async def is_valid(self):
        if not self.email:
            self.errors.append("Email не может быть пустым")
        if not self.email.__contains__("@"):
            self.errors.append("Email должен содержать @")
        if not self.email.__contains__("."):
            self.errors.append("Email должен содержать точку")
        if not self.password:
            self.errors.append("Пароль не может быть пустым")
        if not len(self.password) > 4:
            self.errors.append("Пароль должен быть > 4 символов")
        if not self.errors:
            return True
        return False


class ProfileUpdateForm:
    def __init__(self, request: Request):
        self.request: Request = request
        self.errors: List = []
        self.name: Optional[str] = None
        self.surname: Optional[str] = None
        self.city: Optional[str] = None
        self.phone: Optional[str] = None
        self.english_level: Optional[str] = None
        self.photo: UploadFile = File(default=None)

    async def load_data(self):
        form = await self.request.form()
        self.name = form.get("name")
        self.surname = form.get("surname")
        self.city = form.get("city")
        self.phone = form.get("phone")
        self.english_level = form.get("english_level")
        self.photo = form.get("photo")

    async def is_valid(self):
        if self.phone:
            if len(self.phone) < 10 or len(self.phone) > 15:
                self.errors.append("Длина телефона должна быть от 10 до 15 символов")

            result = re.match(
                r"^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$", self.phone
            )
            if bool(result) is False:
                self.errors.append("Неверный номер телефона")
        if not self.errors:
            return True
        return False


class CollectionForm:
    def __init__(self, request: Request):
        self.request: Request = request
        self.errors: List = []
        self.name: Optional[str] = None
        self.description: Optional[str] = None
        self.photo: UploadFile = File(default=None)

    async def load_data(self):
        form = await self.request.form()
        self.name = form.get("name")
        self.description = form.get("description")
        self.photo = form.get("photo")

    async def is_valid(self):
        if not self.name:
            self.errors.append("Name must be filled")
        if not self.errors:
            return True
        return False


class ChangePassForm:
    def __init__(self, request: Request, hash_pass):
        self.request: Request = request
        self.errors: List = []
        self.old_pass: Optional[str] = None
        self.new_pass: Optional[str] = None
        self.new_pass_repeat: Optional[str] = None
        self.hash_pass: Optional[str] = hash_pass

    async def load_data(self):
        form = await self.request.form()
        self.old_pass = form.get("old_pass")
        self.new_pass = form.get("new_pass")
        self.new_pass_repeat = form.get("new_pass_repeat")

    async def is_valid(self):
        if not self.new_pass:
            self.errors.append("Пароль не может быть пустым")
        if not len(self.new_pass) > 4:
            self.errors.append("Пароль должен быть > 4 символов")
        if self.new_pass != self.new_pass_repeat:
            self.errors.append("Пароли не совпадают")
        if not verify_password(self.old_pass, self.hash_pass):
            self.errors.append("Старый пароль не совпадает")
        if not self.errors:
            return True
        return False
