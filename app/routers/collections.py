from pathlib import Path

from fastapi import APIRouter, Depends, status
from fastapi.templating import Jinja2Templates
from googletrans import Translator
from sqlalchemy.orm import Session
from starlette.requests import Request
from starlette.responses import RedirectResponse

from app import models
from app.database import get_db
from app.forms import CollectionForm
from app.oauth2 import get_current_user
from app.schemas import CollectionCreate, DeleteBody, SaveBody, SaveBodyResponse, DeleteWordBody, SimpleResponse
from app.utils import alphabet

router = APIRouter(prefix="/collection", tags=["Collection"])

BASE_PATH = Path(__file__).resolve().parent.parent
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))


def get_user_from_request(request, db):
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        current_user = None
    return current_user


def get_collection(id: int, db):
    return db.query(models.Collection).filter(models.Collection.id == id).first()


def get_all_collection(db):
    return db.query(models.Collection).all()


def redirect_to_some_page(request, url: str = "root"):
    method = request.method
    if method.lower() == "get":
        status_code = status.HTTP_302_FOUND
    elif method.lower() == "post":
        status_code = status.HTTP_303_SEE_OTHER
    else:
        status_code = status.HTTP_301_MOVED_PERMANENTLY

    if "/" in url:
        return RedirectResponse(url=url, status_code=status_code)

    return RedirectResponse(url=request.url_for(url), status_code=status_code)


@router.get("/all")
async def all_collections(request: Request, db: Session = Depends(get_db)):
    """
    request - base param, request from user
    db - db connection session

    return html page for all collection
    """
    current_user = get_user_from_request(request, db)
    collections = get_all_collection(db)
    return templates.TemplateResponse(
        "all_collections.html", {"request": request, "collections": collections, "user": current_user}
    )


@router.get("/collection/{id}")
async def collection_by_id(request: Request, id: int, db: Session = Depends(get_db)):
    """
    request - base param, request from user
    id - collection id
    db - db connection session

    return html page for collection
    """
    current_user = get_user_from_request(request, db)
    collection_ = get_collection(id=id, db=db)
    return templates.TemplateResponse(
        "collection.html", {"collection": collection_, "user": current_user, "request": request}
    )


@router.get("/create_collection")
async def create_collection_get(request: Request, db: Session = Depends(get_db)):
    """
    request - base param, request from user
    db - db connection session

    return html page for creating collection
    """
    current_user = get_user_from_request(request, db)
    if current_user is None:
        return redirect_to_some_page(request, url="root")

    return templates.TemplateResponse("create_collection.html", {"request": request, "user": current_user})


@router.get("/update_collection/{id}")
async def update_collection_get(request: Request, id: int, db: Session = Depends(get_db)):
    current_user = get_user_from_request(request, db)
    if current_user is None:
        return redirect_to_some_page(request, "all_collections")

    collection = get_collection(id=id, db=db)

    return templates.TemplateResponse(
        "update_collection.html", {"request": request, "collection": collection, "user": current_user}
    )


@router.post("/create_collection", response_model=CollectionCreate)
async def create_collection_post(request: Request, db: Session = Depends(get_db)):
    """
    request - base param, request from user
    db - db connection session

    deleting allowed only for authorized users
    """
    current_user = get_user_from_request(request, db)
    if current_user is None:
        return redirect_to_some_page(request, url="root")

    form = CollectionForm(request)
    await form.load_data()
    print(form.name)
    if not (await form.is_valid()):
        form.__dict__.get("errors").append("Error with name")
        context = form.__dict__
        context["user"] = current_user
        return templates.TemplateResponse("create_collection.html", context, status_code=status.HTTP_400_BAD_REQUEST)

    new_collection = models.Collection(
        name=form.name, description=form.description, user_id=current_user.id, user=current_user
    )
    db.add(new_collection)
    db.commit()
    db.refresh(new_collection)
    return redirect_to_some_page(request, url="all_collections")


@router.post("/delete_collection")
async def delete_collection_post(request: Request, body: DeleteBody, db: Session = Depends(get_db)):
    """
    request - base param, request from user
    body - send dada
    db - db connection session

    deleting allowed only for creators
    """
    current_user = get_user_from_request(request, db)
    if current_user is None:
        return "error"

    collection_ = (
        db.query(models.Collection)
        .filter(models.Collection.id == body.id and models.Collection.user.id == current_user.id)
        .first()
    )

    if collection_:
        db.delete(collection_)
        db.commit()
        print("deleted successfully")
        return "success"

    return "error"


@router.post("/update_collection/{id}")
async def update_collection_post(request: Request, id: int, db: Session = Depends(get_db)):
    """
    request - base param, request from user
    id - collection id
    db - db connection session

    editing allowed only for creators
    """
    collection = get_collection(id=id, db=db)
    current_user = get_user_from_request(request, db)

    if current_user is None:
        return redirect_to_some_page(request, url="root")

    form = CollectionForm(request)
    await form.load_data()
    print(form.name)
    if not (await form.is_valid()):
        form.__dict__.get("errors").append("Error with name")
        context = form.__dict__
        context["user"] = current_user
        return templates.TemplateResponse("create_collection.html", context, status_code=status.HTTP_302_FOUND)
    if collection:
        collection.name = form.name
        collection.description = form.description
        db.add(collection)
        db.commit()
        db.refresh(collection)
        return redirect_to_some_page(request, url="all_collections")
    else:
        return "error"


@router.post("/add_word")
async def add_word(request: Request, body: SaveBody, db: Session = Depends(get_db)):
    """
    request - base param, request from user
    body - send dada
    db - db connection session

    adding only for authorized users
    """
    current_user = get_user_from_request(request, db)
    collection = get_collection(id=body.collection_id, db=db)
    if current_user is None:
        return redirect_to_some_page(request, url="root")

    response_type = "success"
    translator = Translator(service_urls=["translate.googleapis.com"])

    if body.word[0] in alphabet["ru"]:
        word_ru = body.word
        word_en = translator.translate(body.word, dest="en", src="ru").text
    else:
        word_en = body.word
        word_ru = translator.translate(body.word, dest="ru", src="en").text

    word = db.query(models.Word).filter(models.Word.word_ru == word_ru and models.Word.word_en == word_en).first()
    if word is None:
        word = models.Word(word_ru=word_ru, word_en=word_en)
        db.add(word)
        db.commit()
        db.refresh(word)
    else:
        for wic in collection.collection_words:
            if wic.word.id == word.id:
                response = {"type": "not added", "collection_id": collection.id, "word_ru": word_ru, "word_en": word_en}
                return response

    new_collection = models.CollectionWord(collection_id=collection.id, word_id=word.id)
    db.add(new_collection)
    db.commit()
    db.refresh(new_collection)

    response = {
        "type": response_type,
        "collection_id": body.collection_id,
        "word_id": word.id,
        "word_ru": word_ru,
        "word_en": word_en,
    }
    return response


@router.post("/delete_word_from_collection", response_model=SimpleResponse)
async def delete_word_from_collection(request: Request, body: DeleteWordBody, db: Session = Depends(get_db)):
    """
    request - base param, request from user
    body - send dada
    db - db connection session

    deleting words of some collection allowed for creators
    """
    current_user = get_user_from_request(request, db)
    collection = get_collection(id=body.collection_id, db=db)
    bad_response = {"type": "error"}
    if current_user is None:
        return bad_response

    all_words = (
        db.query(models.CollectionWord)
        .filter(
            models.CollectionWord.collection_id == body.collection_id and models.CollectionWord.word_id == body.word_id
        )
        .all()
    )

    for word_collection in all_words:
        if word_collection.word.id == int(body.word_id):
            db.delete(word_collection)
            db.commit()
            response = {"type": "success"}
            return response

    response = {"type": "error"}
    return response
