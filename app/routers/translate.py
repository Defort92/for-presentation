from pathlib import Path

from fastapi import APIRouter, Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from googletrans import Translator
from sqlalchemy.orm import Session
from starlette.requests import Request

from app import models
from app.database import get_db
from app.oauth2 import get_current_user
from app.schemas import TextToTranslate, ToFavorites, FavoriteWordCreate, UserWord

router = APIRouter(prefix="/translate", tags=["Translate"])

BASE_PATH = Path(__file__).resolve().parent.parent
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))


@router.get("/", response_class=HTMLResponse)
async def translate(request: Request, db: Session = Depends(get_db)):
    """return translate page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        current_user = None
    return templates.TemplateResponse("translate.html", {"request": request, "user": current_user})


@router.post("/")
async def translate(text: TextToTranslate):
    """translate words"""
    translator = Translator(service_urls=["translate.googleapis.com"])
    translated_text = translator.translate(text.content, src=text.src, dest=text.dest)
    return translated_text


@router.post("/add_to_favorites")
async def add_to_favorites(request: Request, word: ToFavorites, db: Session = Depends(get_db)):
    """add words to favorites if user is authenticated"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        current_user = None

    if current_user is None:
        return "Not allowed"

    word = FavoriteWordCreate(word_en=word.en, word_ru=word.ru)
    new_word = models.Word(**word.dict())

    if not db.query(models.Word).filter(models.Word.word_en == word.word_en).first():
        db.add(new_word)
        db.commit()
    else:
        new_word = db.query(models.Word).filter(models.Word.word_en == word.word_en).first()

    user_word = UserWord(user_id=current_user.id, word_id=new_word.id)
    new_user_word = models.UserWord(**user_word.dict())

    if (
        not db.query(models.UserWord)
        .filter(models.UserWord.word_id == new_word.id, models.UserWord.user_id == current_user.id)
        .first()
    ):
        new_user_word.count_of_repetitions = 0
        db.add(new_user_word)
        db.commit()
    else:
        return "already in favorites"

    return "success"
