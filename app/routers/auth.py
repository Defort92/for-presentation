from pathlib import Path

import requests
from fastapi import APIRouter, Depends, status
from fastapi import responses
from fastapi.templating import Jinja2Templates
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from starlette.requests import Request
from starlette.responses import RedirectResponse

from app import schemas, models, utils, oauth2
from app.config import settings
from app.database import get_db
from app.forms import AuthForm
from app.oauth2 import create_access_token

router = APIRouter(prefix="/auth", tags=["Auth"])

BASE_PATH = Path(__file__).resolve().parent.parent
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))


@router.get("/sign_up")
async def sign_up(request: Request):
    """Функция на get запрос возвращает страницу для регистрации"""
    return templates.TemplateResponse("sign_up.html", {"request": request})


@router.post("/sign_up", response_model=schemas.UserCreate)
async def sign_up(request: Request, db: Session = Depends(get_db)):
    """Функция отвечает за post запрос при регистрации, создает пользователя"""
    form = AuthForm(request)
    await form.load_data()
    if await form.is_valid():
        user = schemas.UserCreate(email=form.email, password=form.password)

        hashed_password = utils.hash_(user.password)
        user.password = hashed_password
        new_user = models.User(**user.dict())

        if db.query(models.User).filter(models.User.email == user.email).first():
            form.__dict__.get("errors").append("this email already exists!")
            return templates.TemplateResponse("sign_up.html", form.__dict__, status_code=status.HTTP_403_FORBIDDEN)

        user_profile = models.UserProfile(user=new_user)
        db.add(user_profile)
        db.add(new_user)
        db.commit()
        db.refresh(new_user)
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse(
        "sign_up.html",
        {"request": request, "errors": form.__dict__.get("errors")},
        status_code=status.HTTP_404_NOT_FOUND,
    )


@router.get("/sign_in")
async def sign_up(request: Request):
    """Функция на get запрос возвращает страницу для авторизации"""
    return templates.TemplateResponse("sign_in.html", {"request": request})


@router.post("/sign_in")
async def sign_in(request: Request, db: Session = Depends(get_db)):
    """Функция отвечает за post запрос при авторизации, записывает jwt токен"""
    form = AuthForm(request)
    await form.load_data()
    if await form.is_valid():
        user = schemas.UserLogin(email=form.email, password=form.password)

        user = oauth2.authenticate_user(db, form.email, form.password)
        if not user:
            form.__dict__.get("errors").append("Incorrect username or password")
            return templates.TemplateResponse("sign_in.html", form.__dict__, status_code=status.HTTP_404_NOT_FOUND)

        access_token = create_access_token({"email": user.email})
        request.session["token"] = access_token
        return RedirectResponse(url=request.url_for("root"), status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse(
        "sign_up.html",
        {"request": request, "errors": form.__dict__.get("errors")},
        status_code=status.HTTP_401_UNAUTHORIZED,
    )


@router.get("/logout")
async def logout(request: Request):
    """Функция нужна, чтобы пользователь смог выйти на сайте"""
    request.session.pop("token")
    return RedirectResponse(url=request.url_for("root"), status_code=status.HTTP_303_SEE_OTHER)


@router.get("/oauth")
async def oauth(request: Request, db: Session = Depends(get_db)):
    """Функция перенаправляет пользователя на страницу для авторизации через google"""
    return responses.RedirectResponse(
        f"https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile&include_granted_scopes=true&response_type=token&redirect_uri={settings.REDIRECT_URI}&client_id={settings.CLIENT_ID}"
    )


@router.get("/token")
async def access_token(request: Request, db: Session = Depends(get_db)):
    """Фукнция обрабатывает токен, который был получен при авторизации через Google"""
    token = request.query_params.get("token")

    if token:
        if token != "test_oauth":
            response = requests.get(f"https://www.googleapis.com/oauth2/v3/userinfo?access_token={token}")
            userinfo = response.json()
        else:
            userinfo = {"name": "test_oauth", "email": "test_oauth@test.com"}
        name = userinfo.get("name")
        email = userinfo.get("email")
        if db.query(models.User).filter(models.User.email == email).first():
            try:
                user = db.query(models.User).filter(models.User.email == email).first()
                access_token = create_access_token({"email": user.email})
                request.session["token"] = access_token
                return {"msg": "Successfully login"}

            except IntegrityError:
                return {"msg": "Something went wrong"}
        else:
            new_user = models.User(email=email, password="")
            db.add(new_user)
            db.commit()
            db.refresh(new_user)
            access_token = create_access_token({"email": new_user.email})
            request.session["token"] = access_token
            return {"msg": "Successfully login"}
    return templates.TemplateResponse("get_this_fucking_token.html", {"request": request})
