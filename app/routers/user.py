import os
import random
import shutil
from pathlib import Path

from fastapi import Depends, APIRouter
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session
from starlette import status
from starlette.requests import Request
from starlette.responses import RedirectResponse

from app import schemas, models
from app.database import get_db
from app.forms import ProfileUpdateForm, ChangePassForm
from app.oauth2 import get_current_user, get_password_hash
from app.schemas import Word, Scores

router = APIRouter(prefix="/user", tags=["User"])

BASE_PATH = Path(__file__).resolve().parent.parent
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))


@router.get("/profile")
async def profile(request: Request, db: Session = Depends(get_db)):
    """return profile page if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    user_profile = db.query(models.UserProfile).filter(models.UserProfile.user == current_user).first()

    return templates.TemplateResponse(
        "profile.html", {"request": request, "user": current_user, "user_profile": user_profile}
    )


@router.get("/profile/update")
async def profile_update(request: Request, db: Session = Depends(get_db)):
    """return update profile page if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("profile_update.html", {"request": request, "user": current_user})


@router.post("/profile/update", status_code=status.HTTP_202_ACCEPTED)
async def profile_update(request: Request, db: Session = Depends(get_db)):
    """update user profile if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    form = ProfileUpdateForm(request)
    await form.load_data()
    if await form.is_valid():
        user_profile = db.query(models.UserProfile).filter(models.UserProfile.user == current_user).first()
        if user_profile is None:
            user_profile = models.UserProfile(user=current_user)
            db.add(user_profile)
            db.commit()
        user_name = form.name if form.name else user_profile.name
        user_surname = form.surname if form.surname else user_profile.surname
        user_city = form.city if form.city else user_profile.city
        user_phone = form.phone if form.phone else user_profile.phone
        user_english_level = form.english_level if form.english_level else user_profile.english_level
        user_photo = form.photo if form.photo else user_profile.photo
        if user_photo:
            try:
                if not os.path.exists(os.path.join(BASE_PATH, f"static/img/profiles/{current_user.id}/")):
                    os.makedirs(os.path.join(BASE_PATH, f"static/img/profiles/{current_user.id}/"))

                with open(f"app/static/img/profiles/{current_user.id}/" + user_photo.filename, "wb") as image:
                    shutil.copyfileobj(user_photo.file, image)

                url = str(
                    os.path.join(request.url_for("root"), f"static/img/profiles/{current_user.id}/")
                    + user_photo.filename
                )
            except PermissionError:
                url = user_profile.photo
        else:
            url = None
        profile = schemas.UserProfileUpdate(
            name=user_name,
            surname=user_surname,
            city=user_city,
            phone=user_phone,
            english_level=user_english_level,
            photo=url,
        )
        profile_data_dict = profile.dict(exclude_unset=True)
        for key, value in profile_data_dict.items():
            setattr(user_profile, key, value)
        db.commit()
        return RedirectResponse(url="/user/profile", status_code=status.HTTP_303_SEE_OTHER)
    else:
        return templates.TemplateResponse(
            "profile_update.html",
            {"request": request, "user": current_user, "errors": form.__dict__.get("errors")},
            status_code=status.HTTP_400_BAD_REQUEST,
        )


@router.get("/profile/favorites")
async def show_favorites(request: Request, db: Session = Depends(get_db)):
    """return favorites words page if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    favorites = []
    words = db.query(models.UserWord).filter(models.UserWord.user_id == current_user.id).all()
    for word in words:
        favorites.append(db.query(models.Word).filter(models.Word.id == word.word_id).first())

    return templates.TemplateResponse(
        "favorites.html", {"request": request, "user": current_user, "favorites": favorites}
    )


@router.post("/profile/favorites/delete")
async def delete_favorite_word(request: Request, word: Word, db: Session = Depends(get_db)):
    """delete favorites words if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    word = (
        db.query(models.UserWord)
        .filter(models.UserWord.user_id == current_user.id, models.UserWord.word_id == word.id)
        .first()
    )
    db.delete(word)
    db.commit()

    return "success"


@router.get("/profile/favorites/repeat")
async def repeat_favorite_word(request: Request, db: Session = Depends(get_db)):
    """return repeat favorites words page if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    words = db.query(models.UserWord).filter(models.UserWord.user_id == current_user.id).all()
    random.shuffle(words)
    words = words[:5]
    if len(words) < 5:
        return RedirectResponse(url="/user/profile/favorites", status_code=status.HTTP_302_FOUND)

    repeat_words = {}
    for word in words:
        repeat_word = db.query(models.Word).filter(models.Word.id == word.word_id).first()
        repeat_words.update({repeat_word.word_ru: repeat_word.word_en})

    all_words = db.query(models.Word).all()

    return templates.TemplateResponse(
        "repeat_favorites.html",
        {"request": request, "user": current_user, "repeat_words": repeat_words, "all_words": all_words},
    )


@router.post("/profile/scores/add")
async def add_scores(request: Request, score: Scores, db: Session = Depends(get_db)):
    """add scores to user if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    user_profile = db.query(models.UserProfile).filter(models.UserProfile.user_id == current_user.id).first()
    user_profile.score += score.score
    db.commit()


@router.get("/profile/purposes")
async def show_purposes(request: Request, db: Session = Depends(get_db)):
    """return purposes page if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    purposes = db.query(models.Purpose).filter(models.Purpose.user_id == current_user.id).all()
    return templates.TemplateResponse("purposes.html", {"request": request, "user": current_user, "purposes": purposes})


@router.post("/profile/purposes/add")
async def add_purpose(request: Request, purpose: schemas.AddPurpose, db: Session = Depends(get_db)):
    """add purpose if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    purpose = schemas.Purpose(
        description=purpose.description, date=purpose.date, user_id=current_user.id, words_amount=0
    )
    new_purpose = models.Purpose(**purpose.dict())
    db.add(new_purpose)
    db.commit()
    return {"redirect": "/user/profile/purposes"}


@router.post("/profile/purposes/delete")
async def delete_purpose(request: Request, purpose: schemas.DeletePurpose, db: Session = Depends(get_db)):
    """delete purpose if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    db_purpose = (
        db.query(models.Purpose)
        .filter(models.Purpose.id == purpose.id, models.Purpose.user_id == current_user.id)
        .first()
    )
    db.delete(db_purpose)
    db.commit()
    return "success"


@router.post("/profile/purposes/complete")
async def complete_purpose(request: Request, purpose: schemas.DeletePurpose, db: Session = Depends(get_db)):
    """complete purpose if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    db_purpose = (
        db.query(models.Purpose)
        .filter(models.Purpose.id == purpose.id, models.Purpose.user_id == current_user.id)
        .first()
    )
    if db_purpose.completed is True:
        db_purpose.completed = False
    else:
        db_purpose.completed = True
    db.commit()
    return "success"


@router.post("/profile/purposes/change")
async def change_purpose(request: Request, purpose: schemas.ChangePurpose, db: Session = Depends(get_db)):
    """change purpose if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    db_purpose = (
        db.query(models.Purpose)
        .filter(models.Purpose.id == purpose.id, models.Purpose.user_id == current_user.id)
        .first()
    )
    db_purpose.description = purpose.description
    db.commit()
    return "success"


@router.get("/profile/change_pass")
async def get_change_pass(request: Request, db: Session = Depends(get_db)):
    """return change password page if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    favorites = []
    words = db.query(models.UserWord).filter(models.UserWord.user_id == current_user.id).all()
    for word in words:
        favorites.append(db.query(models.Word).filter(models.Word.id == word.word_id).first())

    return templates.TemplateResponse("change_pass.html", {"request": request, "user": current_user})


@router.post("/profile/change_pass")
async def post_change_pass(request: Request, db: Session = Depends(get_db)):
    """change user password if user is authenticated else redirect to sign page"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    form = ChangePassForm(request, current_user.password)
    await form.load_data()
    if await form.is_valid():
        current_user.password = get_password_hash(form.new_pass)
        db.commit()
        return templates.TemplateResponse(
            "change_pass.html", {"request": request, "user": current_user, "success": "Пароль успешно изменен"}
        )

    else:
        return templates.TemplateResponse(
            "change_pass.html",
            {"request": request, "user": current_user, "errors": form.__dict__.get("errors")},
            status_code=status.HTTP_400_BAD_REQUEST,
        )
