from pathlib import Path

from fastapi import APIRouter, Depends, WebSocket, WebSocketDisconnect
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from starlette import status
from starlette.requests import Request
from sqlalchemy.orm import Session
from starlette.responses import Response
from starlette.responses import RedirectResponse

from app.database import get_db
from app.oauth2 import get_current_user
from app.schemas import RegisterValidator
from app.utils import SocketManager

router = APIRouter(prefix="/chat", tags=["Chat"])

BASE_PATH = Path(__file__).resolve().parent.parent
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))

manager = SocketManager()


@router.get("/register", response_class=HTMLResponse)
async def chat_register(request: Request, db: Session = Depends(get_db)):
    """register user to chat"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("chat-register.html", {"request": request, "user": current_user})


@router.get("/", response_class=HTMLResponse)
async def chat(request: Request, db: Session = Depends(get_db)):
    """return chat page if user is authenticated"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        return RedirectResponse(url="/auth/sign_in", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("chat.html", {"request": request, "user": current_user})


@router.websocket("/chat-api")
async def chat_api(websocket: WebSocket):
    """return chat using websockets"""
    sender = websocket.cookies.get("X-Authorization")
    if sender:
        await manager.connect(websocket, sender)
        response = {"sender": sender, "message": "got connected"}
        await manager.broadcast(response)
        try:
            while True:
                data = await websocket.receive_json()
                await manager.broadcast(data)
        except WebSocketDisconnect:
            manager.disconnect(websocket, sender)
            response["message"] = "left"
            await manager.broadcast(response)


@router.get("/api/current_user")
def get_user(request: Request):
    """get current user for chat"""
    return request.cookies.get("X-Authorization")


@router.post("/api/register")
def register_user(response: Response, user: RegisterValidator):
    """register user for chat"""
    response.set_cookie(key="X-Authorization", value=user.name, httponly=True)
