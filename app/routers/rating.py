from pathlib import Path

from fastapi import APIRouter, Depends
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from starlette.requests import Request
from sqlalchemy.orm import Session
from fastapi_utils.tasks import repeat_every
from fastapi_utils.session import FastAPISessionMaker
from sqlalchemy import text

from app import models
from app.database import get_db, SQLALCHEMY_DATABASE_URL
from app.oauth2 import get_current_user

router = APIRouter(prefix="/rating", tags=["Rating"])

BASE_PATH = Path(__file__).resolve().parent.parent
templates = Jinja2Templates(directory=str(BASE_PATH / "templates"))
sessionmaker = FastAPISessionMaker(SQLALCHEMY_DATABASE_URL)


@router.get("/users", response_class=HTMLResponse)
async def rating_user(request: Request, db: Session = Depends(get_db)):
    """Функция возвращает html страницу с рейтингом пользователей"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        current_user = None
    users = (
        db.query(models.UserRating, models.UserProfile)
        .order_by(models.UserRating.place)
        .filter(models.UserRating.user_id == models.UserProfile.user_id)
        .all()
    )
    return templates.TemplateResponse("rating_users.html", {"request": request, "user": current_user, "users": users})


@router.get("/words", response_class=HTMLResponse)
async def rating_word(request: Request, db: Session = Depends(get_db)):
    """Функция возвращает html страницу с рейтингом слов"""
    token = request.session.get("token")
    if token:
        current_user = get_current_user(token, db)
    else:
        current_user = None
    words = db.query(models.WordRating).order_by(models.WordRating.place).all()
    return templates.TemplateResponse("rating_words.html", {"request": request, "user": current_user, "words": words})


@router.on_event("startup")
@repeat_every(seconds=60 * 60 * 24, raise_exceptions=True)
def refresh_raitings() -> None:
    """Функция каждые 24 обновляет в бд таблицы с рейтингом пользователей и слов"""
    with sessionmaker.context_session() as db:
        refresh_users(db)
        refresh_words(db)


def refresh_users(db):
    """Функция обновляет в бд таблицу рейтинга пользователей"""
    db.query(models.UserRating).delete()
    db.commit()
    users = (
        db.query(models.UserProfile)
        .order_by(models.UserProfile.score.desc())
        .filter(models.UserProfile.score != None)
        .limit(10)
        .all()
    )
    for i in range(len(users)):
        new_user = models.UserRating(user_id=users[i].user_id, place=i + 1)
        db.add(new_user)
        db.commit()


def refresh_words(db):
    """Функция обновляет в бд таблицу рейтинга слов"""
    db.query(models.WordRating).delete()
    db.commit()
    sql = text("SELECT word_id from user_word GROUP BY word_id order by count(user_id) DESC limit 10")
    result = db.execute(sql)
    i = 0
    for row in result:
        new_word = models.WordRating(word_id=row[0], place=i + 1)
        i += 1
        db.add(new_word)
        db.commit()
