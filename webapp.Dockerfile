FROM python:3.8 as app

ENV PYTHONUNBUFFERED 1

COPY requirements.txt .

RUN pip install -r requirements.txt && poetry config virtualenvs.create false

WORKDIR /app
COPY pyproject.toml .
RUN poetry install --no-dev
RUN pip install googletrans==3.1.0a0

COPY . .

CMD alembic upgrade head && uvicorn app.main:app --host=0.0.0.0 --port=${PORT:-8000}
