"""set default score to 0

Revision ID: 4eac8059eaba
Revises: 33b79745dc34
Create Date: 2022-05-31 12:43:47.293743

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4eac8059eaba"
down_revision = "33b79745dc34"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("user_profiles", "score", default=0)


def downgrade():
    op.alter_column("user_profiles", "score", default=None)
