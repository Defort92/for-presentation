"""Add columns to user model

Revision ID: 5e118e631050
Revises: 415e148e6e6d
Create Date: 2022-05-18 19:16:56.907639

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "5e118e631050"
down_revision = "415e148e6e6d"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("user_profiles", sa.Column("city", sa.String(50)))
    op.add_column("user_profiles", sa.Column("phone", sa.String(15)))
    op.add_column("user_profiles", sa.Column("photo", sa.String(200)))
    op.alter_column("user_profiles", "english_level", nullable=True)


def downgrade():
    op.drop_column("user_profiles", "city")
    op.drop_column("user_profiles", "phone")
    op.drop_column("user_profiles", "photo")
    op.alter_column("user_profiles", "english_level", nullable=False)
