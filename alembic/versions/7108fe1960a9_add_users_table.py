"""add users table

Revision ID: 7108fe1960a9
Revises: 
Create Date: 2022-04-11 19:13:44.234464

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "7108fe1960a9"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "users",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("email", sa.String(), nullable=False),
        sa.Column("password", sa.String(), nullable=False),
        sa.Column("created_at", sa.TIMESTAMP(timezone=True), nullable=False, server_default=sa.text("now()")),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("email"),
    )


def downgrade():
    op.drop_table("users")
