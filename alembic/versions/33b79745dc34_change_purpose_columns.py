"""change purpose columns

Revision ID: 33b79745dc34
Revises: 5e118e631050
Create Date: 2022-05-28 10:07:25.797913

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "33b79745dc34"
down_revision = "5e118e631050"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("purposes", "words_amount", nullable=True)
    op.alter_column("purposes", "date", nullable=True)


def downgrade():
    op.alter_column("purposes", "words_amount", nullable=False)
    op.alter_column("purposes", "date", nullable=False)
